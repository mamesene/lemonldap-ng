package Lemonldap::NG::Common::Logger::Null;

our $VERSION = '2.1.0';

sub new {
    return bless {}, shift;
}

sub AUTOLOAD {
    return 1;
}

1;
