# IO::Handle filter. Used to transform HTML::Template on the fly.
package Lemonldap::NG::Common::IO::Filter;
use strict;
use IO::File;
use Symbol;

#our @ISA = ('IO::File');

sub new {
    my ( $class, $file, $opt ) = @_;
    $opt->{_i} = ( ref $file ? $file : IO::File->new($file) )
      or die("Unable to build IO::File object $!");
    my $self = ref $class ? $class : bless gensym, $class;
    tie( *$self, $class, $opt );
    return $self;
}

sub TIEHANDLE {
    my ( $class, $data ) = @_;
    return bless( $data, $class );
}

sub READLINE {
    my ($self) = shift;
    my $res = $self->{_i}->getline;
    foreach my $key ( keys %$self ) {
        next if ( $key eq '_i' );
        if ( ref( $self->{$key} ) eq 'CODE' ) {
            $res =~ s/__LLNG_${key}__/$self->{$key}->()/gse;
        }
        elsif ( ref $self->{$key} eq 'ARRAY' ) {
            next;
        }
        elsif ( ref $self->{$key} ) {
            local $/ = undef;
            $res =~ s/__LLNG_${key}__/$self->{$key}->getline/gse;
        }
    }

    # Parse strings after code/IO
    foreach my $key ( keys %$self ) {
        die "Undefined value for __LLNG_${key}__ substitution"
          unless $self->{$key};
        my $v =
          ( ref $self->{$key} and ref $self->{$key} eq 'ARRAY' )
          ? $self->{$key}
          : [ $self->{$key} ];
        $v = join "\n",
          map { ref $_ ? () : qq'<TMPL_INCLUDE NAME="$_.tpl">' } @$v;
        $res =~ s/__LLNG_${key}__/$v/gs;
    }
    return $res;
}

sub DESTROY {
    my ($self) = @_;
    $self->close() if ( ref($self) eq 'SCALAR' );
}

sub AUTOLOAD {
    no strict;
    my $self = shift;
    $AUTOLOAD =~ s/^.*:://;
    $AUTOLOAD = lc $AUTOLOAD;
    return tied( ${$self} )->{_i}->$AUTOLOAD(@_);
}

1;
__END__
=head1 NAME

Lemonldap::NG::Common::IO::Filter - IO::Handle filter

=head1 SYNOPSIS

  use HTML::Template;
  my $fh = Lemonldap::NG::Common::IO::Filter->new(
    'template.tpl',
    {
      # Replace all __LLNG_AUTH__ by:
      # <TMPL_INCLUDE NAME="login.tpl">
      AUTH => 'login',
      # Replace all __LLNG_CODE__ by the result of the given function
      CODE => sub {return "INCLUDED STRING"}
    }
  );
  my $h = HTML::Template->new( filehandle => $fh );
  print $h->output;

Input:

  <html><body>
  __LLNG_AUTH__
  <hr>
  __LLNG_CODE__
  </body></html>

Output:

  <html><body>
  <TMPL_INCLUDE NAME="login.tpl">
  <hr>
  INCLUDED STRING
  </body></html>

Same but with a L<IO::Handle> file:

  use HTML::Template;
  my $file = IO::File->new('test.tpl');
  my $fh = Lemonldap::NG::Common::IO::Filter->new_from_io(
    $file,
    {
      # Replace all __LLNG_AUTH__ by:
      # <TMPL_INCLUDE NAME="login.tpl">
      AUTH => 'login',
      # Replace all __LLNG_CODE__ by the result of the given function
      CODE => sub {return "INCLUDED STRING"}
    }
  );
  my $h = HTML::Template->new( filehandle => $fh );
  print $h->output;

Or with an array:

  use HTML::Template;
  my $fh = Lemonldap::NG::Common::IO::Filter->new_from_io(
    'template.tpl',
    {
      # Replace all __LLNG_AUTH__ by:
      # <TMPL_INCLUDE NAME="login.tpl"> <TMPL_INCLUDE NAME="login2.tpl">
      AUTH => [ 'login', 'login2' ],
    }
  );
  my $h = HTML::Template->new( filehandle => $fh );
  print $h->output;

=head1 DESCRIPTION

IO::Handle filter used to transform HTML::Template files on the fly.

=head1 AUTHORS

=over

=item LemonLDAP::NG team L<http://lemonldap-ng.org/team>

=back

=head1 BUG REPORT

Use OW2 system to report bug or ask for features:
L<https://gitlab.ow2.org/lemonldap-ng/lemonldap-ng/issues>

=head1 DOWNLOAD

Lemonldap::NG is available at
L<http://forge.objectweb.org/project/showfiles.php?group_id=274>

=head1 COPYRIGHT AND LICENSE

See COPYING file for details.

This library is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see L<http://www.gnu.org/licenses/>.

=cut
