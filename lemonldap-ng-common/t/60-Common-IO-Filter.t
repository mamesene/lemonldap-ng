use Test::More tests => 10;
use strict;
use HTML::Template;
use IO::String;

use_ok('Lemonldap::NG::Common::IO::Filter');

my ( $b, $t );

# Template name
ok(
    $b = Lemonldap::NG::Common::IO::Filter->new(
        't/test.tpl', { FORM => 't/inc' }
    ),
    'Build IO filter (file.tpl)'
);
ok( $t = HTML::Template->new( filehandle => $b ),
    'Build HTML::Template object' );
ok( $t->output =~ /XX\s+YY\s+ZZ/s, 'Substitution works' );

# Code ref
my $s = IO::String->new('XX __LLNG_AUTH__ ZZ');
ok(
    $b = Lemonldap::NG::Common::IO::Filter->new(
        $s,
        {
            AUTH => sub { 'YY' }
        }
    ),
    'Build IO filter (code ref)'
);
ok( $t = HTML::Template->new( filehandle => $b ),
    'Build HTML::Template object' );
ok( $t->output eq 'XX YY ZZ', 'Substitution works' );

# IO ref
ok(
    $b = Lemonldap::NG::Common::IO::Filter->new(
        't/test.tpl', { FORM => IO::File->new('t/inc.tpl'), AUTH => 't/inc' }
    ),
    'Build IO filter (IO ref)'
);
ok( $t = HTML::Template->new( filehandle => $b ),
    'Build HTML::Template object' );
ok( $t->output =~ /XX\s+YY\s+ZZ/s, 'Substitution works' );
