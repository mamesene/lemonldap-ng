package Lemonldap::NG::Portal::Main::Auth;

use strict;
use Mouse;

our $VERSION = '2.1.0';

extends 'Lemonldap::NG::Portal::Main::Plugin';

# PROPERTIES

has authnLevel => ( is => 'rw' );

1;
