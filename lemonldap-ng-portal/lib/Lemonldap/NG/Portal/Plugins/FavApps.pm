package Lemonldap::NG::Portal::Plugins::FavApps;

use strict;
use Mouse;
use JSON qw(from_json to_json);

our $VERSION = '2.1.0';

extends 'Lemonldap::NG::Portal::Main::Plugin';

# INITIALIZATION
has rule => ( is => 'rw', default => sub { 1 } );

sub init {
    my $self = shift;
    my $hd   = $self->p->HANDLER;

    $self->addAuthRoute( favapps => 'register', ['POST'] );
    $self->addAuthRoute( favapps => 'read',     ['GET'] );
    $self->addAuthRoute( favapps => 'reset',    ['DELETE'] );

    # Parse activation rule
    $self->logger->debug(
        "FavApps activation rule -> " . $self->conf->{portalDisplayFavApps} );
    my $rule =
      $hd->buildSub( $hd->substitute( $self->conf->{portalDisplayFavApps} ) );
    unless ($rule) {
        $self->error(
            "Bad FavApps activation rule -> " . $hd->tsv->{jail}->error );
        return 0;
    }
    $self->rule($rule);

    return 1;
}

# RUNNING METHODS
sub register {
    my ( $self, $req ) = @_;
    my $user = $req->userData->{ $self->conf->{whatToTrace} };

    # Check activation rule
    unless ( $self->rule->( $req, $req->userData ) ) {
        $self->userLogger->warn(
                'FavApps requested by an unauthorized user ('
              . $req->{user}
              . ')' );
        $self->logger->debug('FavApps not authorized');
        return [
            200,
            [
                'Content-Type'   => 'application/json',
                'Content-Length' => 11,
            ],
            ['{"error":0}']
        ];
    }

    my ( $uri, $result );
    unless ( $uri = $req->param('uri') ) {
        return $self->p->sendError( $req, 'Missing App. URI', 400 );
    }
    $self->logger->debug("Favorite application URI received : $uri");
    my $id   = $req->param('id')   || '';
    my $logo = $req->param('logo') || '';
    my $desc = $req->param('desc') || '';
    my $name = $req->param('name') || '';

    # Read existing favorite Apps
    $self->logger->debug("Looking for $user favorite Apps...");
    my $_favApps;
    if ( $req->userData->{_favApps} ) {
        $_favApps = eval {
            from_json( $req->userData->{_favApps}, { allow_nonref => 1 } );
        };
        if ($@) {
            $self->logger->error("Corrupted session (_favApps): $@");
            return $self->p->sendError( $req, "Corrupted session", 500 );
        }
    }
    else {
        $self->logger->debug("No favorite Apps found for $user");
        $_favApps = [];
    }

    # Append or remove favorite application
    my $nbrApps = @$_favApps;
    $self->logger->debug("$nbrApps favorite Apps found");
    if ( $nbrApps && $self->_isFavApp( $_favApps, $uri ) ) {
        $_favApps = $self->_removeFavApp( $_favApps, $uri );
        $self->p->updatePersistentSession( $req,
            { _favApps => to_json($_favApps) } );
        $result = '{"result":0}';
        $self->userLogger->notice(
            "Favorite apps deletion of $uri succeeds for $user");
    }
    else {
        if ( $nbrApps < $self->conf->{favAppsMaxNumber} ) {
            $_favApps =
              $self->_appendFavApp( $_favApps, $uri, $id, $logo, $desc, $name );
            $self->p->updatePersistentSession( $req,
                { _favApps => to_json($_favApps) } );
            $result = '{"result":1}';
            $self->userLogger->notice(
                "Favorite apps registration of $uri succeeds for $user");
        }
        else {
            $result = '{"error":1}';
            $self->userLogger->notice(
                "Max number of favorite apps reached for $user");
        }
    }
    return [
        200,
        [
            'Content-Type'   => 'application/json',
            'Content-Length' => length($result),
        ],
        [$result]
    ];
}

sub read {
    my ( $self, $req ) = @_;
    my $user = $req->userData->{ $self->conf->{whatToTrace} };

    # Read existing favorite Apps
    $self->logger->debug("Looking for favorite Apps...");
    my $_favApps;
    if ( $req->userData->{_favApps} ) {
        $_favApps = eval {
            from_json( $req->userData->{_favApps}, { allow_nonref => 1 } );
        };
        if ($@) {
            $self->logger->error("Corrupted session (_favApps): $@");
            return $self->p->sendError( $req, "Corrupted session", 500 );
        }
    }
    else {
        $self->logger->debug("No favorite Apps found");
        $_favApps = [];
    }

    # Serialize data
    my $data = to_json( {
            result => 1,
            apps   => $_favApps
        }
    );
    $self->logger->debug(
        scalar @$_favApps . " favorite Apps found for $user" );

    return [
        200,
        [
            'Content-Type'   => 'application/json',
            'Content-Length' => length($data),
        ],
        [$data]
    ];
}

sub reset {
    my ( $self, $req ) = @_;
    my $user = $req->userData->{ $self->conf->{whatToTrace} };
    $self->p->updatePersistentSession( $req, { _favApps => '' } );
    $self->userLogger->notice("$user favorite Apps. RESET");

    return [
        200,
        [
            'Content-Type'   => 'application/json',
            'Content-Length' => 12,
        ],
        ['{"result":1}']
    ];
}

sub _isFavApp {
    my ( $self, $_favApps, $uri ) = @_;
    my $test = 0;
    foreach (@$_favApps) {
        if ( $_->{appuri} eq $uri ) {
            $test = 1;
            last;
        }
    }
    $self->logger->debug("App. already registered? $test");
    return $test;
}

sub _appendFavApp {
    my ( $self, $_favApps, $uri, $id, $logo, $desc, $name ) = @_;
    push @$_favApps,
      {
        appuri  => $uri,
        appid   => $id,
        applogo => $logo,
        appdesc => $desc,
        appname => $name
      };
    $self->logger->debug("App. $uri appended");
    return $_favApps;
}

sub _removeFavApp {
    my ( $self, $_favApps, $uri ) = @_;
    @$_favApps = grep { $_->{appuri} ne $uri } @$_favApps;
    $self->logger->debug("App. $uri removed");
    return $_favApps;
}

1;
