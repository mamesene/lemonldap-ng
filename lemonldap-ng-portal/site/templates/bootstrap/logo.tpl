<div class="card">
  <div class="form">
    <TMPL_IF NAME="CUSTOM_LOGO">
        <img src="<TMPL_VAR NAME="STATIC_PREFIX"><TMPL_VAR NAME="CUSTOM_LOGO">" class="img-thumbnail" />
    </TMPL_IF>

    <div class="buttons">
    <button type="submit" class="btn btn-success">
      <span class="fa fa-sign-in"></span>
      <span trspan="connect">Connect</span>
    </button>
    </div>
  </div>
</div>
